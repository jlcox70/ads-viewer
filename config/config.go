package config

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

func init() {
	currentDir, err := os.Getwd()
	if err != nil {
		fmt.Println("Error getting current directory: ", err)
		return
	}
	configPath := currentDir
	viper.AddConfigPath(configPath)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	err = viper.ReadInConfig()
	if err != nil {
		fmt.Println("Error reading config file: ", err)
		return
	}
}

func GetDatabaseConfig() {
	var config DatabaseConfig
	err := viper.UnmarshalKey("database", &config)
	if err != nil {
		fmt.Println("Error unmarshalling config file: ", err)
		return
	}
	fmt.Println(config)
}

type DatabaseConfig struct {
	Host     string `mapstructure:"host"`
	Port     string `mapstructure:"port"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
	Database string `mapstructure:"database"`
}

func GetServerConfig() {
	var config ServerConfig
	err := viper.UnmarshalKey("server", &config)
	if err != nil {
		fmt.Println("Error unmarshalling config file: ", err)
		return
	}
}

type ServerConfig struct {
	Host string `mapstructure:"host"`
	Port string `mapstructure:"port"`
}
