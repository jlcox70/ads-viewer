package models

type Position struct {
	Callsign     string  `json:"callsign"`
	HexIdent     string  `json:"hex_ident"`
	Latitude     float64 `json:"latitude"`
	Longitude    float64 `json:"longitude"`
	Altitude     float64 `json:"altitude"`
	Track        int     `json:"track"`
	Speed        int     `json:"ground_speed"`
	VerticalRate int     `json:"vertical_rate"`
}
