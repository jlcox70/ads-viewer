package models

type Track struct {
	Track    int    `json:"track"`
	HexIdent string `json:"hexIdent"`
}
