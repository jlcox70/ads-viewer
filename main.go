package main

import (
	"adsb_api/api"
	"adsb_api/db"
	"adsb_api/handlers"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigFile("/config/config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("Error reading config file: ", err)

	}
	config := db.GetDatabaseConfig()

	if err := db.InitConfig("/config/config.yaml"); err != nil {
		log.Fatal("Error initializing config: ", err)
	}

	if err := db.InitDB(config); err != nil {
		log.Fatal("Error initializing database: ", err)
	}

	apiConfig := api.GetServerConfig()
	r := mux.NewRouter()
	r.HandleFunc(apiConfig.Prefix+"points", handlers.GetPointsHandler).Methods("GET")
	r.HandleFunc(apiConfig.Prefix+"position", handlers.GetPositionHandler).Methods("GET")
	r.HandleFunc(apiConfig.Prefix+"points/{flight}/{hex_ident}", handlers.GetPointsFlightHandler).Methods("GET")
	r.HandleFunc(apiConfig.Prefix+"track/{hex_ident}", handlers.GetTrackHandler).Methods("GET")

	r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		template, err := route.GetPathTemplate()
		if err == nil {
			log.Println("Route:", template)
		}
		return nil
	})
	log.Println("Server listening on port " + apiConfig.Port + ", with prefix " + apiConfig.Prefix)
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":"+apiConfig.Port, nil))
}
