package api

import (
	"github.com/spf13/viper"
)

type ServerConfig struct {
	Port   string
	Prefix string
}

func InitConfig(filepath string) error {
	viper.SetConfigFile(filepath)
	if err := viper.ReadInConfig(); err != nil {
		return err
	}
	return viper.ReadInConfig()
}

func GetServerConfig() ServerConfig {
	return ServerConfig{
		Port:   viper.GetString("server.port"),
		Prefix: viper.GetString("server.prefix"),
	}
}
