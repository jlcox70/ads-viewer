package handlers

import (
	"adsb_api/db"
	"adsb_api/models"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

func GetTrackHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	hex_ident := vars["hex_ident"]
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	track, err := fetchTrackFromDatabase(hex_ident)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	json.NewEncoder(w).Encode(track)
}

func fetchTrackFromDatabase(hex_ident string) ([]models.Track, error) {
	query := "Select hex_ident,track FROM adsb.last_track "
	query += "where hex_ident ilike '%" + hex_ident + "%' "
	rows, err := db.DB.Queryx(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var tracks []models.Track
	for rows.Next() {
		var track models.Track
		if err := rows.Scan(&track.HexIdent, &track.Track); err != nil {
			return nil, err
		}
		tracks = append(tracks, track)
	}
	return tracks, nil
}
