package handlers

import (
	"adsb_api/db"
	"adsb_api/models"
	"encoding/json"
	"net/http"
)

func GetPositionHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// var:= mux.Vars(r)
	Position, err := fetchPositionFromDatabase()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	json.NewEncoder(w).Encode(Position)
}

func fetchPositionFromDatabase() ([]models.Position, error) {
	query := "SELECT callsign, hex_ident, lon, lat, altitude, track, ground_speed, vertical_rate FROM adsb.last_tracks_120_seconds"
	rows, err := db.DB.Queryx(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var positions []models.Position
	for rows.Next() {
		var position models.Position
		if err := rows.Scan(&position.Callsign, &position.HexIdent, &position.Longitude, &position.Latitude, &position.Altitude, &position.Track, &position.Speed, &position.VerticalRate); err != nil {
			return nil, err
		}
		positions = append(positions, position)
	}
	return positions, nil
}
