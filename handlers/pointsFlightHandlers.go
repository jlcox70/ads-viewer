package handlers

import (
	"adsb_api/db"
	"adsb_api/models"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

func GetPointsFlightHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	flight := vars["flight"]
	hex_ident := vars["hex_ident"]
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	points, err := fetchPointsFlightFromDatabase(flight, hex_ident)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var result [][]float64
	for _, point := range points {
		result = append(result, []float64{point.Latitude, point.Longitude})
	}
	json.NewEncoder(w).Encode(result)
}

func fetchPointsFlightFromDatabase(flight string, hex_ident string) ([]models.Point, error) {
	query := "Select lat,lon FROM adsb.locations_with_callsigns_last_24 "
	query += "where hex_ident ilike '%" + hex_ident + "%' "
	query += "and callsign ilike '%" + flight + "%' "
	query += "and parsed_time > (NOW() - INTERVAL '30 minutes') "
	query += "order by parsed_time asc;"
	rows, err := db.DB.Queryx(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var points []models.Point
	for rows.Next() {
		var point models.Point
		if err := rows.Scan(&point.Latitude, &point.Longitude); err != nil {
			return nil, err
		}
		points = append(points, point)
	}
	return points, nil
}
