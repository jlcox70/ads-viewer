package handlers

import (
	"adsb_api/db"
	"adsb_api/models"
	"encoding/json"
	"net/http"
)

func GetPointsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	points, err := fetchPointsFromDatabase()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var result [][]float64
	for _, point := range points {
		result = append(result, []float64{point.Latitude, point.Longitude})
	}
	json.NewEncoder(w).Encode(result)
}

func fetchPointsFromDatabase() ([]models.Point, error) {
	config := db.GetDatabaseConfig()
	query := "Select lat,lon FROM " + config.TableName + "  where lat <> 0::double precision and lon <>0::double precision  order by hex_ident desc limit 1000"
	rows, err := db.DB.Queryx(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var points []models.Point
	for rows.Next() {
		var point models.Point
		if err := rows.Scan(&point.Latitude, &point.Longitude); err != nil {
			return nil, err
		}
		points = append(points, point)
	}
	return points, nil
}
