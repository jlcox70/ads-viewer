package db

import (
	"github.com/spf13/viper"
)

type DatabaseConfig struct {
	Driver           string `mapstructure:"driver"`
	ConnectionString string `mapstructure:"connection_string"`
	TableName        string `mapstructure:"table_name"`
	Host             string `mapstructure:"host"`
}

func InitConfig(filepath string) error {
	viper.SetConfigFile(filepath)
	return viper.ReadInConfig()
}
func GetDatabaseConfig() DatabaseConfig {
	return DatabaseConfig{
		Driver:           viper.GetString("database.driver"),
		ConnectionString: viper.GetString("database.connection_string"),
		TableName:        viper.GetString("database.table_name"),
		Host:             viper.GetString("database.host"),
	}
}
