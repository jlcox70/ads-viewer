package db

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var DB *sqlx.DB

func InitDB(config DatabaseConfig) error {
	fmt.Println(config)
	connectionString := config.ConnectionString + " host=" + config.Host
	fmt.Println(connectionString)
	db, err := sqlx.Connect(config.Driver, connectionString)
	if err != nil {
		return err
	}
	DB = db
	return nil
}
