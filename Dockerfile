FROM golang as builder
WORKDIR /app
COPY . .
RUN go install \
    && CGO_ENABLED=0 go build -o bin/api ./

FROM scratch
WORKDIR /var/runtime
COPY --from=builder /app/bin/api /api
COPY config/config.yaml /config.yaml
ENTRYPOINT [ "/api" ]    
